//
//  main.swift
//  控制语句
//
//  Created by durban.zhang on 2018/5/5.
//  Copyright © 2018年 durban.zhang. All rights reserved.
//

import Foundation

print("2、Swift控制语句")

var scores = [90, 80, 100, 43, 34, 56, 89]

var maxScore = 0
var minScore = 0
var avgScore = 0
var sumScore = 0
var count = scores.count

for score in scores {
    print("score is \(score)")
    sumScore += score
    if (maxScore == 0 || maxScore < score) {
        maxScore = score
    }
    
    if (minScore == 0 || minScore > score) {
        minScore = score
    }
}
print("avgScore = \(sumScore / count)")
print("sumScore = \(sumScore)")
print("minScore = \(minScore)")
print("maxScore = \(maxScore)")

var index = 0

repeat {
    print("do...while score is \(scores[index])")
    
    index += 1
} while(index < count)

index = 0
while(index < count) {
    print("while... score is \(scores[index])")
    index += 1
}

let deviceType = "iOS"
switch deviceType {
case "iOS":
    print("iOS Device")
case "Android":
    print("Android Device")
case "Window":
    print("Window Device")
default:
    print("No Device")
}
